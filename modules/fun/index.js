import say from "./say";

export default function (command, message, args) {
    if (["say", "repeat"].includes(command)) say(message, args);
    // quote
    // joke
    // d20
    // flip
    // remindme
}

/**
 * Repeat after user
 * @param message
 * @param args
 */
export default function (message, args) {
    const sayMessage = args.join(" ");
    message.delete().catch(() => {});
    if (message.member.roles.some(r => ["admin", "mod"].includes(r.name)))
        return message.channel.send(sayMessage);
    message.channel.send(`${message.member.nickname} made me say: ${sayMessage}`);
}

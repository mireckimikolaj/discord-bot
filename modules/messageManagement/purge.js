/**
 * This command removes all messages from all users in the channel, up to 100
 * @param message
 * @param args
 * @returns {Promise<Message | Message[]>}
 */
export default async function (message, args) {
    const deleteCount = parseInt(args[0], 10);

    if (!deleteCount || deleteCount < 1 || deleteCount > 100)
        return message.reply("Please provide a number between 1 and 100 for the number of messages to delete");

    const fetched = await message.channel.fetchMessages({limit: deleteCount + 1});
    message.channel.bulkDelete(fetched)
        .catch(error => message.reply(`Couldn't delete messages because of: ${error}`));
}

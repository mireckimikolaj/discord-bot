import purge from "./purge";

export default function (command, message, args) {
    if (["purge", "cleanup"].includes(command)) purge(message, args);
    // purge bots
    // purge images
}

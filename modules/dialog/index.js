import dialogflow from "./dialogflow";

export default function (message, app, apiai) {
    dialogflow(message, app, apiai);
}

/**
 * Contact Dialogflow API and respond to user with some small talk
 * @param message
 * @param app
 * @param dialogflow
 * @returns {Promise<void>}
 */
export default async function ping(message, app, dialogflow) {
    const user = message.author.id;
    const result = await new Promise(function (resolve, reject) {
        const request = dialogflow.textRequest(message.cleanContent.replace(`@${app.user.username} `, ""), {
            sessionId: user
        });
        request.on('response', function (response) {
            resolve(response.result.fulfillment.speech);
        });

        request.on('error', function (error) {
            resolve(null);
        });

        request.end();
    });
    if (result) {
        message.reply(result);
    } else {
        message.reply("beep boop, something's not working");
    }
}

import confession from "./confession";

export default function (command, message) {
    if (["confession"].includes(command)) confession(message);
}

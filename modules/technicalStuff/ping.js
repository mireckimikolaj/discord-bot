/**
 * Ping Pong! Returns latency
 * @param message
 * @returns {Promise<Message | Message[]>}
 */
export default async function (message, app) {
    const m = await message.channel.send("Ping?");
    m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(app.ping)}ms`);
}

import info from "./info";
import ping from "./ping";

export default function (command, message, app, Discord) {
    if (["info"].includes(command)) info(message, app, Discord);
    if (["ping", "latency"].includes(command)) ping(message, app);
    //diagnose
    //uptime
}

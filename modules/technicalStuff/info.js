/**
 * Returns info about the bot
 * @param message
 * @param app
 * @param Discord
 * @returns {Promise<void>}
 */
export default async function (message, app, Discord) {
    const embed = new Discord.RichEmbed()
        .setTitle("Hello there")
        .setColor("#dd2e44")
        .setDescription("This is the main body of text, it can hold 2048 characters.")
        .setFooter("Visit www.\<domainfollows\>.com for more info")
        .addField("This is a field title, it can hold 256 characters",
            "This is a field value, it can hold 1024 characters.")
        /*
         * Inline fields may not display as inline if the thumbnail and/or image is too big.
         */
        .addField("Inline Field", "They can also be inline.", true)
        /*
         * Blank field, useful to create some space.
         */
        .addBlankField(true)
        .addField("Inline Field 3", "You can have a maximum of 25 fields.", true);

    message.channel.send({embed});
}

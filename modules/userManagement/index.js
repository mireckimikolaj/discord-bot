import ban from "./ban";
import kick from "./kick";

export default function (command, message) {
    if (["kick"].includes(command)) kick(message);
    if (["ban", "destroy"].includes(command)) ban(message);
    // warn
    // mute
    // members - list members
    // log user
}

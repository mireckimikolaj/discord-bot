import Discord from "discord.js";
import * as dotenv from "dotenv";
import apiai from "apiai";

import {
    dialog,
    fun,
    games,
    messageManagement,
    music,
    privateMessaging,
    raidProtection,
    rankSystem,
    technicalStuff,
    userManagement
} from "./modules"

dotenv.config();
const {
    DIALOGFLOW_TOKEN,
    PREFIX,
    DISCORD_TOKEN
} = process.env;

const app = new Discord.Client();

app.on("ready", () => {
    console.log(`Bot has started, with ${app.users.size} users, in ${app.channels.size} channels of ${app.guilds.size} guilds.`);
    app.user.setActivity(`Serving ${app.guilds.size} servers`);
});

app.on("guildCreate", guild => {
    console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
    app.user.setActivity(`Serving ${app.guilds.size} servers`);
});

app.on("guildDelete", guild => {
    console.log(`I have been removed from: ${guild.name} (id: ${guild.id})`);
    app.user.setActivity(`Serving ${app.guilds.size} servers`);
});

app.on("message", async message => {
    if (message.author.bot) return;

    if (message.content.indexOf(PREFIX) === 0 && message.channel.type === "dm") {
        // command
        const args = message.content.slice(PREFIX.length).trim().split(/ +/g);
        const command = args.shift().toLowerCase();
    } else if (message.cleanContent.startsWith("@" + app.user.username) || message.channel.type === "dm") {
        // dialogflow
        dialog(message, app, apiai(DIALOGFLOW_TOKEN));
    } else {
        if (message.content.indexOf(PREFIX) !== 0) return;

        const args = message.content.slice(PREFIX.length).trim().split(/ +/g);
        const command = args.shift().toLowerCase();

        if (message.channel.type === "dm") {
            privateMessaging(command, message, args);
        } else {
            fun(command, message, args);
            games(command, message, args);
            messageManagement(command, message, args);
            music(command, message, args);
            raidProtection(command, message, args);
            rankSystem(command, message, args);
            technicalStuff(command, message, app, Discord, args);
            userManagement(command, message, args);
        }
    }
});

app.login(`${DISCORD_TOKEN}`);

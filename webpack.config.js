const path = require("path");

module.exports = (env, argv) => {
    return ({
        entry: {
            server: "./index.js",
        },
        output: {
            path: path.join(__dirname, "dist"),
            publicPath: "/",
            filename: "[name].js"
        },
        mode: argv.mode,
        target: "node",
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                }
            ]
        }
    })
};

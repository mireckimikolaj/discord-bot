# Discord Bot

Just another discord bot.

---

- [How to use](#how-to-use)
    - [Setup](#setup)

## How to use

### Setup

This project uses NPM in version 12 or higher.

To start the project locally run

```bash
npm install
npm start
```
